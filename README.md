# Main Orginizational Repo for Libre Space Foundation #

In this repo you can find issues that LSF uses to track high level non-repo-specific work.
This repository also contains the source code for documentation of Libre Space Foundation.


## Styling guide ##

reStructeredText in this repository shall follow this styling guide for source code consistency.
These rules are **not** checked as part of quality gating with a CI job.


### Sections ###

The succession of headings shall follow the [Python Developer's Guide for documentation](https://devguide.python.org/documentation/markup/#sections):

  * 1st level, `#` with overline
  * 2nd level, `*` with overline
  * 3rd level, `=`
  * 4th level, `-`
  * 5th level, `^`
  * 6th level, `"`


### Lists ###

Numbered lists shall be auto-numbered using the `#.` sign, unless required otherwise.
Unnumbered lists shall only use `*` sign, unless required otherwise.


### Paragraphs ###

All sentences shall end with a punctuation mark and a newline character; only a single sentence can exist on each line.
Lines shall be wrapped to 79 characters (GNU formatting style).
