########
Meetings
########

Meetings can be called either on-demand or regularly.
To avoid any miscommunication and facilitate syncing between meeting
participants, it is recommended to follow the following guidelines.

********************
Calling for meetings
********************

#. | **Define a goal**
   | Every meeting needs to have a goal.
     The goal should be clear in order to give direction to the discussion.
#. | **Define the agenda**
   | All meetings must have an agenda.
     The agenda helps participants focus and keep the meeting within its time
     constraints.
     The agenda should at least contain:

   #. The **topic** and **subtopics** to be discussed
   #. Possible **preparation** required by participants to be able to attend

#. | **Define participants**
   | The participants should be separated between:
   
   #. **Mandatory** participants
   #. **Optional** participants

#. | **Define time**
   | The meeting should take place within a specific time-frame.
     The time-frame should be such that all mandatory participants can attend.
     Try not to exceed 1h to avoid fatigue.
     Use free/busy information, if available, to minimizes chances of
     rescheduling.
     The meeting should be scheduled far enough into the future to let
     participants respond in time for their availability.
     Always add some extra time; meetings usually run overtime.

#. | **Define location**
   | The location, either physical or virtual, should be defined.
   
#. | **Set reminders**
   | Participants should be reminded in some way when a meeting is starting
     soon.

#. | **Make resources available**
   | Any resources needed to execute the meeting (documents, pages, links,
     etc.) should be available to the participants either directly or as links.


Calendar invitations
====================

The most common way to call for meetings is through calendar event invitations.
The recommended mapping between meeting properties and calendar invitations is:

+--------------------+---------------------------------------+
|Invitation          | Meeting                               |
+====================+=======================================+
|Start time          | Start time                            |
+--------------------+---------------------------------------+
| End time           | End time                              |
+--------------------+---------------------------------------+
| Title              | Agenda topic                          |
+--------------------+---------------------------------------+
|Description         | Agenda                                |
+--------------------+---------------------------------------+
| Location           | Location                              |
+--------------------+---------------------------------------+
| Required attendees | Mandatory participants                |
+--------------------+---------------------------------------+
| Optional attendees | Optional participants                 |
+--------------------+---------------------------------------+
| Reminders          | Reminders (e.g. 15 mins before start) |
+--------------------+---------------------------------------+


Virtual meetings
================

Virtual meetings, preferably, take place on the Big Blue Button instance of
Libre Space Foundation.
The BBB service is integrated to Libre Space Foundation Cloud.
Personal and shared meeting rooms can be managed through `the BBB tab of the
LSF Cloud <https://cloud.libre.space/apps/bbb/>`_.
Access to created meeting rooms can be set to 'Public' or 'Internal', with
various combinations for guest access (password, waiting room, etc.).
When creating a new meeting room, set the appropriate access control settings,
based on the privacy-sensitivity of the subject and the audience expected to be
invited.
To join meeting rooms set to be accessed internally, users are required to
log-in using their LSF Cloud credentials.
Avoid joining as a guest when you do have an LSF Cloud account and log-in
instead so that your user's avatar is properly displayed in the participants
list.
Always make sure that the audio setting are correct and the audio setup is
functioning properly before joining a virtual meeting.
A lot of frustration can be avoided by trying to briefly join with your devices
(e.g. laptop, mobile phone) beforehand, in order to test that everything works
and familiarize yourself with the platform.
It is possible to join BBB virtual meetings using any modern web browser,
including mobile web browsers; thus there is no need to install any specific
mobile application.


*************************
Responding to invitations
*************************

Participants are expected to respond to invitation as soon as possible.
Always check your calendar to avoid double booking and always account for
possible time needed to prepare before responding to an invitation.
It is acceptable for participants to completely deny attending or ask to
reschedule a meeting.
In any case, always reply with the reason.


*******************
Initiating meetings
*******************

To start a meeting, the following guidelines should be followed:

#. | **Small talk for 5 minutes, if possible**
   | This helps people to start talking and also allows time for everybody to
     gather.
#. | **Assign roles**
   | The following roles shall be assigned to people at the start of the
     meeting:

   #. **Leader** - The person who leads the discussion; usually the person who
      called the meeting
   #. **Notekeeper** - The person who keeps minutes of the meeting
   #. **Kanban master** - If the meeting involves a Kanban board review, the
      person who goes over the Kanban cards (e.g. GitLab issues)
   #. **Timekeeper** - The person who makes sure that the meeting runs on
      schedule
   #. **Participant** - Anyone who is invited and attends the meeting

      
*************
Participating
*************

To make meetings more efficient, the following guidelines should be followed
after meeting starts:

#. Always try to stick to the agenda and the meeting goal, as defined by the
   organizer
#. Come up with clear action items which can be later documented as tasks for
   people undertake
#. Avoid overrunning the scheduled meeting time; people may have scheduled
   other tasks after the meeting

   
********************
Post-meeting actions
********************

After the end of the meeting, the meeting notes should be published by the
**Notekeeper** and all action items should be filed as tasks to the task track
system (e.g. GitLab issues) by all participants assigned to them.


********************
Regular LSF meetings
********************

Morning get-together
====================

This meeting takes place every week from Tuesday to Friday, between 10:00am and
11:00am, Athens local time.
It is open to any discussions and attendance is optional.
No Leader is required.
The meeting is also exempted from the rule to keep notes, thus a Notekeeper is
not necessary.
At Friday there is an all day gathering at HQ
(`HSGR <https://www.hackerspace.gr/>`_).


Evaluation
----------

The meeting is currently running an experimental period.
The parameters of the morning get-together meeting shall be evaluated on 28th
of Septemeber, 2023.


Monday Priorities Meeting
=========================

This meeting takes place every week at Monday, between 10:00am and
11:00am, Athens local time.
This meeting is split in two parts.
In the first half, project champions report to the head of operations the
status of project (Done, Doing, To Do).
Leader of meeting and Kanban master is the head of operations.
Notekeeper is each project champion and the notes are placed in Project
Tracking issue board under the Core repository.
Participants are all the core members.
This meeting sets the priorities for the week on an organization level.
In the second half of meeting, each core member reports on individual
priorities without details like said the projects that want to condribute.
During this process, individual priorities might shift, following the needs
of the organization.
Leader could be anyone of the team.
Notekeeper is each contributor for their priorities.


Evaluation
----------

The meeting is durring an evaluation period.
The parameters of the morning get-together meeting shall be evaluated on 28th
of Septemeber, 2023.
