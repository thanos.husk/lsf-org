################
Event Management
################

********
Overview
********

LSF contributors regularly attend or organize meetings to promote our projects
and engage with the broader community.
This page describes the process which should be applied for all events.

*******
Prepare
*******

Think about what do you want to get out of the event participation.
Plan carefully and as much in advance as possible.
If needed, feel free to notify or ask for help on the `community forums`_.

Check `previous presentations`_ and you can also use `LSF logos (and related
designs)`_.

.. _community forums: http://community.libre.space/
.. _previous presentations: https://cloud.libre.space/s/FoEwgAckb9XK9Qz
.. _LSF logos (and related designs): https://cloud.libre.space/s/ibMbgj9JaidaYAg

*******
Propose
*******

Make sure to follow the instructions of the event and propose your
participation (booth, talk, session, paper etc) as soon as possible.
In case you need financial (or other) help with attending and representing LSF
at the event, make sure to use `this form`_ to request for assistance.

.. _this form: https://docs.google.com/forms/d/e/1FAIpQLSdG0wPkQ1aenfbBezqqiqrVY3M5Y-gw-5Aj7MYz7d_ju2FXlQ/viewform

******
Attend
******

Be present, be active, keep notes on contacts and meetings, as well as document
interesting things you see and might be of interest in the LSF community.

******
Report
******

Post back at the community forums for how the event went, as well as any
recordings made, contacts acquired and follow-ups needed.

*************
Reimbursement
*************

Scan all your receipts in a single PDF, then sum the amount and convert it to
EUR, and post the file and details to receipts@libre.space.
