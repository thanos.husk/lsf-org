########
Glossary
########

The purpose of this document is to provide a reference for acronyms and terms
used in the Libre Space Foundation community and contributors may cross
reference any of the supplyied terms.

.. glossary::

 AB
  Abstract

 ABCL
  As-built Configuration Data List
 
 AC
  Alternating Current
 
 ACI
  Adjacent Channel Interference

 ACS
  Attitude Control System

 ACU
  Antenna Control Unit

 ADC
  Analog to Digital Converter

 ADCS
  Attitude Determination and Control System

 AFSK
  Audio Frequency Shift Keying

 AFT
  Abbreviated Functional Test

 AGC
  Automatic Gain Control

 AGPL
  Affero General Public Licence

 AI
  Artificial Intelligence

 AIT
  Assembly Integration and Testing

 AIV
  Assembly Integration and Verification

 AM
  Amplitude Modulation

 AMSAT
  Amateur Satellite

 AOCS
  Attitude and Orbit Control System

 AOS
  May refer to:
  Acquisition Of Signal or, 
  Advanced Orbiting Systems

 AoV
  Angle of View

 API
  Application Programming Interface

 APT
  Automatic Picture Transmission

 ARM
  Advanced RISC Machines

 ARRL
  American Radio Relay League

 ARTES
  Advanced Research in Telecommunications Systems

 ASIC
  Application-Specific Integrated Circuit

 ASW
  Address and Synchronization Word

 AXI
  Advanced eXtensible Interface

 B2B
  Business to Business

 B2C
  Business to Consumer

 BaLun
  Balanced to Unbalanced

 BMU
  Battery Management Unit

 BER
  Bit Error Rate

 BOM
  Bill Of Materials

 BR
  Brochure

 BPF
  Band Pass Filter

 BPbis
  Bundle Protocol

 BPSK
  Binary Phase Shift Keying

 BSD
  Berkeley Software Distribution

 CAD
  Computer Aided Design

 CAN
  Controller Area Network

 CAN-FD
  Controller Area Network Flexible Data-rate

 CC-BY-SA
  Creative Commons Attribution-ShareAlike

 CCD
  Contract Closure Documentation

 CCI
  Co-channel Interference

 CCSDS
  Consultative Committee for Space Data Systems

 CDR
  Critical Design Review

 CEPT
  European Conference of Postal and Telecommunications Administrations

 CERN
  Center for European Nuclear Research

 CERN-OHL
  :term:`CERN` Open Hardware Licence

 CFDP
  :term:`CCSDS` File Delivery Protocol

 CI
  Continuous Integration

 CI/CD
  Continuous Integration/Continuous Development

 CIDL
  Configuration Item Data List

 CNC
  Computer numerical control

 COLDSUN
  COmmunications reLay for Deep-Sea Underwater Networks

 COMMS
  Communications

 CONOPS
  Concept of Operations

 COTS
  Commercial off the Self

 CPLD
  Complex Programmable Logic Device

 CPU
  Central Processing Unit

 CRC
  Cyclic Redundancy Check

 CSP
  Content Security Policy

 CSS
  Cascading Style Sheet

 CV
  Cirriculum Vitae

 CVCM
  Collected Volatile Condensable Material

 CW
  Continuous Wave

 D
  Deliverable

 DAC
  Digital to Analog Converter

 dB
  Decibel

 DB
  Database

 DC
  Direct Current

 DDR
  Double Data Rate

 DL
  Deep Learning

 DM
  Development Model

 DMSA
  Deployable Multifuction Solar Array

 DP
  Data Package

 DR
  Design Review

 DSN
  Deep Space Network

 DSP
  Digital Signal Processing

 DTN
  Delay and Disruption-Tolerant Networking

 DUT
  Device Under Test

 DUV
  Data Under Voice

 DVB-RCS
  Digital Video Broadcasting - Return Channel via Satellite

 DVB-S2
  Digital Video Broadcasting - Satellite -Second Generation

 Eb/N0
  Energy per bit to noise power spectral density ratio

 E2E
  End-to-End

 ECC
  Error-correcting code

 ECCS
  Emergency Communication Cell over Satellite

 ECSS
  European Cooperation for Space Standardization

 EDAC
  Error Detection And Correction

 EDEN
  :term:`EGSE` Data Exchange Network

 EDRS
  European Data Relay Satellite System

 EDV
  Energy Detection Value

 EGSE
  Electrical Ground Support Equipment

 EIRP
  Equivalent isotropically radiated power

 EM
  May refer to:
  a) Engineering Model 
  b) Electrical Model

 EMC
  ElectroMagnetic Compatibility

 EMI
  ElectroMagnetic Interference

 eMMC
  embedded Multi-Media Controller

 EMS
  Emergency Management Services

 ENBW
  Equivalent noise bandwidth

 ENOB
  Effective number of bits

 EO
  Earth Observing

 EPS
  Electrical Power System

 Es/N0
  symbol energy to noise ratio

 ESA
  European Space Agency

 ESOC
  European Space Operations Center

 ESR
  Executive Summary Report

 ESSB
  :term:`ESA` Standardization Steering Board

 ESTEC
  European Space Research and Technology Center

 ETA
  Estimated Time of Arrival

 EUR
  Euro

 FAR
  Final Acceptance Review

 FDIR
  Failure Detection, Isolation and Recover

 FEA
  Finite Element Analysis

 FEC
  Forward Error Correction

 FITS
  Flexible Image Transport System

 FFT
  Fast Fourier Transform

 FM
  May refer to:
  Flight Model or,
  Frequency Modulation

 FMC
  FPGA Mazzanine Card

 FOC
  Full Operational Capability

 FOSDEM
  Free and Open Source Developers European Meeting

 FOR
  Frame Of Reference

 FORTH
  Foundation for Research and Technology Hellas

 FOV
  Field of View

 FP
  Final Presentation

 FPGA
  Field Programmable Gate Array

 FR
  Final Report

 FSK
  Frequency Shift Keying

 G/T
  Gain-to-noise-temperature

 GB
  Giga Byte

 GCC
  GNU Compiler Collection

 GEO
  Geostationary Equatorial Orbit

 GESTRA
  German Experimental Space Surveillance and Tracking Radar

 GFSK
  Gaussian Frequency Shift Keying

 GHz
  Giga Hertz

 GLONASS
  Global Navigation Satellite System

 GMAT
  General Mission Analysis Tool

 GmbH
  Gesellschaft mit beschränkter Haftung: German for a limited liability company

 GNSS
  Global Navigation Satellite System

 GPCPU
  General Purpose :term:`CPU`

 GNU
  GNU's Not Unix

 GPL
  :term:`GNU` General Public License

 GPS
  Global Positioning System

 GPSd
  Global Positioning System Data

 GPSDO
  :term:`GPS`-Disciplined Oscillator

 GRAVES
  Grand Réseau Adapté à la Veille Spatiale

 gRPC
  gRPC Remote Procedure Calls

 GS
  Ground Station

 GSE
  Ground Support Equipment

 GSPS
  Giga Samples per Second

 GSoC
  Google Summer of Code

 GUI
  Graphical User Interface

 HAL
  Hardware Abstraction Layer

 HDF5
  Hierarchical Data Format

 HDL
  Hardware Description Language

 HEEQ
  Heliocentric Earth Equatorial

 HEO
  Highly Elliplical Orbit

 HFT
  Hidden Factors as Topics

 HPA
  High Power Amplifier

 HPC
  May refer to:
  a) High-performance Computing
  b) High Pin Count

 HQ
  Head-Quarters

 HTML
  Hyper Text Markup Language

 HW
  Hardware

 I2C
  Inter-Integrated Circuit

 IaC
  Infrastructure as Code

 IARU
  International Amateur Radio Union

 IC
  Integrated Circuit

 ICT
  Information and Communication Technology

 IDE
  Integrated Development Environment

 IDLE
  Integrated Development Environment

 IEEE
  Institute of Electrical and Electronics Engineers

 IESL
  Institue of Electronic Structure and Laser

 IF
  Intermediate Frequency

 IFFT
  Inverse Fast Fourier Tranform

 IO
  Input/Output

 IOCR
  In-Orbit Commissioning Review

 IOD
  In-Orbit Demostration

 IOT
  Internet Of Things

 IQ
  In-phase/Quadrature

 IP
  Implementation Plan

 IP3
  Third-order intercept point

 IPND
  IP-based Neighbor Discovery

 IPv4
  Internet Protocol version 4

 IPv6
  Internet Protocol version 6

 ISI
  Intersymbol Interference

 ISM
  Industrial, Scientific, and Medical

 ISON
  International Scientific Optical Network

 ISS
  International Space Station

 ITT
  Invitation To Tender

 ITU
  International Telecommunication Union

 ITU-R
  International Telecommunication Union - Radiocommunications

 ITU RR
  ITU Radio Regulations

 JAXA
  Japan Aerospace Exporation Agency

 JPL
  Jet Propulsion Laboratory

 KO
  Kick-Off

 KOM
  Kick-Off Meeting

 LCNS
  Lunar Communication and Navigation System

 LDPC
  Low-Density Parity Check

 LGPL
  GNU Lesser General Public License

 LEDSAT
  LED-based Small SATellite

 LEO
  Low Earth Orbit

 LEOP
  Launch and Early Orbit Phase

 LFSR
  Linear-Feedback Shift Register

 LNA
  Low Noise Amplifier

 LO
  Local Oscillator

 LOS
  May refer to
  a) Line of Sight 
  b) Loss Of Signal

 LRPT
  Low-Resolution Picture Transmission

 LRR
  Laser Retro Reflector

 LSF
  Libre Space Foundation

 LSTM
  Long Short-Term Memory

 LSTN
  Library Space Technology Network

 LTAN
  Local Time of the Ascending Node

 LVDS
  Low Voltage Differential Signalling

 LVTTL
  Low Voltage TTL

 M2M
  Machine To Machine

 MAIV
  Manufacturing, Assembly, Integration and Verification

 MB
  Mega Byte

 MCC
  Mission Control Center

 MCMC
  Markov Chain Monte Carlo

 MCOR
  Mission Close-Out Review

 MCT
  Mission Control Software

 MCU
  Micro-Controller Unit

 MDS
  Minimum Detectable Signal

 MEMS
  Micro-Electro-Mechanical Systems

 MEO
  Medium Earth Orbit

 MEX
  Mars Express

 MGSE
  Mechanical Ground Support Equipment

 MHz
  Mega Hertz

 MIB
  Mission Information Base

 MIMO
  Multiple-Input, Multiple-Output

 MIT
  Massachusetts Institute of Technology

 ML
  Machine Learning

 MLRR
  Modulated Laser Retro-Reflector

 MoM
  Minutes of Meeting

 MOOC
  Massive Open Online Course

 MPL
  Mozilla Public License

 MPPT
  Maximum Power Point Tracker

 MPR
  Monthly Progress Reports

 MRG
  Microelectronics Research Group

 MRR
  Modulated Laser Retro-Reflector

 MS
  Milestone

 MSK
  Minimum Shift Keying

 MSPS
  Mega Samples per Symbol

 MSS
  Mobile Satellite Service

 NASA
  National Aeronautics and Space Administration

 NEC2
  Numerical Electromagnetics Code Version 2

 NF
  Noise Figure

 NIST
  National Institute of Standards and Technology

 NORAD
  North American Air Defense Command

 NRZ-M
  Non-Return-to-Zero Mark

 NS-3
  Network Simulator 3

 NSL
  NearSpace Launch

 NTE
  Nanosatellite Tracking Experiment

 OBC
  On-Board Computer

 OBSW
  On-Board Software

 OCS
  Orbit Control System

 ODM
  Orbit Data Message

 OEM
  May refer to:
  a) Original Equipment Manufacturer
  b) Orbit Ephemeris Message

 OOL
  Out-of-Limits

 OOT
  Out-of-Tree (GNURadio Out-of-tree module)

 OpenEMS
  Open Source Energy Management System

 OpenSatCom
  Open Satellite Communications

 OPS
  (Space) Operations

 OS
  Operating System

 OS3
  Open Source Satellite Simulator

 OSCW
  Open Source Cubesat Workshop

 OSDLP
  Open Space Data Link Protocol

 OSI
  May refer to
  a) Open Systems Interconnection
  b) Open Source Initiative

 OTA
  Optical Tube Assembly

 OTP
  One time programmable

 PA
  Power Amplifier

 PAPR
  Peak to Average Power Ration

 PC
  May refer to:
  a) Project Coordinator
  b) Personal Computer

 PCB
  Printed Circuit Board

 PCDU
  Power Conditioning and Distribution Unit

 PCIe
  Peripheral Component Interconnect Express

 PDM
  Product Data Management

 PDR
  Preliminary Design Review

 PL
  Processing Logic

 PLL
  Phased Locked Loop

 PLM
  Product Lifecycle Management

 PMIC
  Power management integrated circuits

 PNT
  Positioning, Navigation and Timing

 PoC
  Proof of Concept

 PoE
  Power over Ethernet

 POSIX
  Portable Operating System Interface for Unix

 PQ
  PocketQube

 PSK
  Phase-Shift Keying

 PSLV
  Polar Satellite Launch Vehicle

 PV
  Photovoltaics

 PWM
  Pulse-width Modulation

 PWP
  Project Web Page

 QA
  Quality Assurance

 QPSK
  Quadrature Phase Shift Keying

 QSFP
  Quad Small Form-factor  Pluggable transceiver

 R&D
  Research & Development

 RAAN
  Rigth Ascension of the Ascending Node

 RAM
  Random Access Memory

 RD
  Reference Document

 REST
  Reprentational State Transfer

 RF
  Radio Frequency

 RFMC
  RF Mezzanine Card

 RFSoC
  RF System on Chip

 RHCP
  Right Hand Circular Polarized

 RILDOS
  Radio with Identity and Location Data for Operations and SSA

 RPC
  Remote Procedure Calls

 RR
  Requirements Review

 RSSI
  Received Signal Strength Indication

 RTC
  Real Time Clock

 RTOS
  Real Time Operating System

 RW
  Reaction Wheel

 RX
  Receiver/Reception

 SANA
  Space Assigned Numbers Authority

 SATA
  Serial Advanced Technology Attachment (hard disk interface)

 SATCOM
  Satellite Communication

 SatNOGS
  Satellite Network Open Ground-Station

 SAW
  Surface Acoustic Wave

 SBC
  Single Board Computer

 SC
  Spacecraft

 SDA
  Space Data Association

 SDLS
  Space Data Link Security

 SDN
  Software Defined Networking

 SDR
  May refer to:
  a) Software Defined Radio
  b) System Desing Review

 SGP4
  Standard General Perturbations Satellite Orbit Model 4

 SEE
  Single-event effects

 SESP
  Simulation for European Space Programmes

 SEU
  Single-event upset

 SFCG
  Space Frequency Coordination Group

 SFPT
  Satellite Functions and Performance Test

 SHF
  Super High Frequency (3-30 GHz)

 SIDLOC
  Satellite Identification and Localisation

 SiDs
  Simple Downlink Share Convention Protocol

 SigMF
  Signal Metadata Format

 SIMD
  Single Instruction, Multiple Data

 SLE
  Space Link Emulator

 SMA
  SubMiniature version A

 SMT
  Surface Mount Technology

 SNR
  Signal-to-noise ratio

 SNS
  Satellite Network Simulator

 SNS3
  Satellite Network Simulator

 SoC
  System On a Chip

 SOS
  Space Operation Service

 SoW
  Statement of Work

 SPENVIS
  Space Environment, Effect and Education System

 SPI
  Serial Peripheral Interface

 SPOC
  Spacecraft Operation Center

 SPP
  Space Packet Protocol

 SQNR
  Signal-to-quantization-noise ratio

 SRF
  Software Reuse File

 SRS
  Space Research Service

 SSA
  Space Situational Awareness

 SSN
  Space Surveillance Network

 SSO
  Sun-Synchronous Orbit

 SST
  Space Surveillance and Tracking

 STM
  Space Traffic Management

 SW
  Software

 SWR
  Standing Wave Radio

 TAS
  Technical Achievement Summary

 TBD
  To Be Described

 TC
  Tele-command

 TC&C
  Tele-command & Control

 TCP
  Transmission Control Protocol

 TCXO
  Temperature-compensated crystal oscillator

 TDM
  Tracking Data Message

 TDMA
  Time Division Multiple Access

 TDP
  Technical Data Package

 TID
  Total Ionizing Dose

 TJD
  Truncated Julian Day

 TLE
  Two Line Element set

 TM
  Telemetry

 TML
  Total Mass Loss

 TMR
  Triple Mode Redundancy

 TMTC
  Telemetry & Tele-control

 TN
  Technical Note

 TRL
  Technology Readiness Level

 TRR
  Test Readiness Review

 TT&C
  Telemetry, Tracking and Command

 TTL
  Transistor-transistor logic

 TVAC
  Thermal Vacuum Chamber

 TWT
  Travelling-wave Tube

 TX
  Transmit

 UART
  Universal Asynchronous Receiver - Transmitter

 UAS
  Unmanned Aircraft Systems

 UHF
  Ultra High Frequency (0.3 - 3 GHz)

 UI
  User Interface

 UNOOSA
  United Nations Office for Outer Space Affairs

 USB
  Universal Serial Bus

 USLP
  Unified Space Link Protocol

 USRP
  Universal Software Radio Peripheral

 UTC
  Universal Time Co-ordinated

 UX
  User Experience

 V&V
  Verification and Validation

 VBat
  Voltage of Battery

 VCO
  Voltage - Controlled Oscillator

 VGA
  Variable Gain Amplifier

 VHF
  Very High Frequency (30 - 300 MHz)

 VNA
  Vector Network Analyzer

 VOLK
  Vectorized Library of Kernels

 VHDL
  VHSIC Hardware Description  Language

 VPL
  Visual Programming Language

 WBS
  Work Breakdown Structure

 WP
  Work Package

 WPD
  Work Package Description

 WRC
  World Radiocommunication Conferences

 YAML
  YAML Ain’t Markup Language

 ZMQ
  ZeroMQ
