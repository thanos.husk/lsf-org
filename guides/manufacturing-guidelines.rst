########################
Manufacturing Guidelines
########################

.. contents::
   :local:
   :backlinks: none

****************************
Engineering Drawing Language  
****************************

The GD&T symbolic language (a measurement-driven system of engineering  
drawing generation) is used in all LSF’s engineering drawings both for in house 
manufactured and outsourced parts. This way it is ensured that all 
parts/components are manufactured to the quality standards that are set. 
By using this language, a standard ecosystem of procedures and tools revolving 
around the GD&T is created, making the verification process of each part easier.

.. image:: images/SidlocGDT.png
   :height: 350px
   :alt: Ariane sidloc GD&T

******************************
Mechanical Tolerance Standards
******************************

Unless otherwise specified, the DIN ISO 2768-mK standard is followed for 
dimensioning and tolerancing of the parts. Some parts required for LSF’s 
projects are miniature-size, thus depending on the application we may use more 
fine tolerances, while always following the GD&T guidelines. 

.. image:: images/sidlocGranite.jpg
   :height: 300px
   :alt: Ariane sidloc on the granite plate

.. image:: images/RFFEantenna.png
   :height: 300px
   :alt: Ariane sidloc oRFFE connector

**************************
NASA workmanship standards
**************************

Wherever applicable, the NASA workmanship standards are followed. 
The full guide can be found below.
https://workmanship.nasa.gov/lib/insp/2%20books/frameset.html

*********************
Wiring and Harnessing
*********************

=================
FM wire harnesses
=================

The wire harnesses for the flight models are wrapped with Dyneema string as per
the workmanship standard section:
CABLE AND HARNESS GENERAL REQUIREMENTS. 
Depending on the application, closed, or flat stitching is chosen. A flat 
stitching example is shown below for the Ariane - SIDLOC flight model.

.. image:: images/FMWireHarness.png
   :height: 350px
   :alt: Wire Harness method

.. image:: images/SidlocHarness.png
   :height: 350px
   :alt: Ariane sidloc Wire Harness

===========================
Wire Stripping and Crimping
===========================

To comply with the NASA workmanship standards, special tools have been
bought for wire striping and crimping. As a team, we have standardized the
MOLEX-PicoBlade and the Hirose - DF11 connectors as the preferred Wire-to-Wire
connectors for all our subsystems. This standardization alongside the specific
tools acquired (thermal wire striper and PicoBlade / DF11 specific crimper),
makes the striping and crimping procedure repeatable and leaves little
room for error. Before crimping, each wire is thoroughly inspected with a
microscope to ensure its optimal state and compliance with the workmanship
standard. The thermal wire striper used alongside the PicoBlade crimper and
the inspection microscope can be seen below.
Each harness is tested by a specific jig that connects the cable to operational
voltage and maximum current. Then pass or fail the test according to criteria
that are defined by the application.
Use the NASA workmanship standard and connector manufacturer datasheet as 
reference, sections:

.. image:: images/PIcobladecrimp.jpg
   :height: 300px
   :alt: Pcoblade crimper tool

.. image:: images/thermalstriper.jpg
   :height: 300px
   :alt: Thermal wire striper tool
   
*************************************************************
Application and Use of adhesives, adhesive tapes and coatings
*************************************************************

==================
Electronics Gluing
==================

Some electronics components cannot withstand the vibrations of a space launch
relying only on the solder joint between them and the PCB. Thus, epoxy glue is
used according to the workmanship standard to permanently attach them to thePCB.

+---------------------------------+---------------------------------+
| Part #1                         | Part #2                         |
+---------------------------------+---------------------------------+
| Male Wire to Wire connector     | PCB                             |
+---------------------------------+---------------------------------+
| Male Wire to Wire connector     | Wire side during integration    |
+---------------------------------+---------------------------------+
|EEE with footprint area > 48mm^2 | PCB                             |
+---------------------------------+---------------------------------+
|On board bateries                | PCB                             |
+---------------------------------+---------------------------------+
|RF Connectors                    | PCB                             |
+---------------------------------+---------------------------------+
|RF Connectors                    | Wire side during integration    |
+---------------------------------+---------------------------------+

The following steps should be done before the gluing / coating procedure starts:

#. At least 5 working days before the assembly, the health of the adhesives
   should be checked. In each adhesive, check the following and verify with
   the datasheet.

   * Expiration date
   * Date of opening
   * Color
   * Consistency
   * Viscosity

#. Check with the electronics team that the EEE components can withstand the
   temperatures needed for the epoxy to cure. Some glued assemblies may be
   subjected to high temperatures (40+°C) in order to shorten the curing time. 
   The curing temperature shall not exceed the store temperature of the EEE
   components.

#. Apply a small adhesive amount to a draft part and perform a gluing / coating
   test. Check / perform the following:

   * Establish the curing temperature by reading the manual
   * Establish the curing time by reading the manual 
   * Confirm with the electronics team that the EEE components can withstand 
     these conditions. The curing temperature shall not exceed the EEE 
     component's storing temperature
   * Document the curing temperature and time in the project's logs
   * Cure the draft part 
   * Check the color
   * Ensure proper adhesion
   * Take high resolution pictures
   * Catalog the photos in the logs for future reference.

#. If the curing test is succesfull proceed with the next steps.
#. Mark in a picture the epoxy application spots.
  
   .. image:: images/CommsEpoxy.png
      :height: 300px
      :alt: Epoxy application spots

#. Apply the epoxy with a plastic stick.
  
   .. image:: images/EpoxyApplication.png
      :height: 300px
      :alt: Epoxy application

#. Cure the  epoxy by following the steps below. 

   * Establish the curing temperature by reading the manual
   * Establish the curing time by reading the manual 
   * Confirm with the electronics team that the EEE components can withstand 
     these conditions. The curing temperature shall not exceed the EEE 
     component's storing temperature
   * Document the curing temperature and curing time in the project's logs
   * Cure the draft part 
   * Check the color
   * Ensure proper adhesion
   * Take high resolution pictures
   * Catalog the photos in the logs for future reference.

#. After the epoxy has cured, preceed with the conformal coating application.

=============================
Electronics Conformal Coating
=============================

In order to protect the populated PCB’s, a UV fluorescent conformal coating is
applied in a closed and clean container.
The fluorescence helps with visual inspection and provides an easy way of
ensuring the proper application of the coating. 
Before applying the coating, certain components need to be masked with kapton
tape. (e.g. Wire to Wire connectors). The masking procedure should be performed
in a clean environment with extra caution in order not to damage the PCBs.
A list of the components that should be masked can be seen below:

.. image:: images/SidlocMasking.jpg
   :height: 250px
   :alt: Ariane sidloc Wire Harness

#. Every type of mating connectors (eg. Picoblade)
#. Exposed solder pads that require soldering after this step (eg. during 
   integration).
#. Small slots in the PCBs that are going to be mated with other PCBs 
   (eg. Qubik Side panel slots in the base plate PCB)
#. Switches
#. Thermal knives

The application should be done by following the coating's guidelines in the
datasheet.

.. image:: images/Conformal.png
   :height: 300px
   :alt: Ariane sidloc Wire Harness

The most commonly used adhesives can be seen in the following table.

+---------------------------------+---------------------------------+
| Adhesive                        | Part Number                     |
+---------------------------------+---------------------------------+
| Thread lock                     | Loctite 243 blue                |
+---------------------------------+---------------------------------+
| Epoxy                           | Loctite 9466                    |
+---------------------------------+---------------------------------+
| Conformal Coating               | MG Chemicals 422B               |
+---------------------------------+---------------------------------+
| Electronics Glue                | Electrolube 1LABLSMA10S/11      |
+---------------------------------+---------------------------------+

==============
Adhesive tapes
==============

TBD
