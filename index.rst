####################################
Libre Space Foundation documentation
####################################

.. toctree::
   :maxdepth: 2
   :caption: 🚀 Projects
   :hidden:

   projects/what-is-an-lsf-project
   Management <projects/project-management>
   Portfolio <projects/projects-portfolio>
   Joining <projects/project-joining>
   Proposing <projects/project-proposing>
   projects/roles
   projects/procurement

.. toctree::
   :maxdepth: 2
   :caption: 👥 People
   :hidden:

   Overview <people/people-overview>
   people/board
   people/core-contributors
   people/paid-contributors

.. toctree::
   :maxdepth: 2
   :caption: 🐝 Collaboration
   :hidden:

   collaboration/decision-making
   collaboration/meetings
   collaboration/conflict-resolution
   collaboration/code-of-conduct

.. toctree::
   :maxdepth: 2
   :caption: 📨 Communication
   :hidden:

   Channels <communication/communication-channels>
   communication/event-management

.. toctree::
   :maxdepth: 2
   :caption: 🪄 Guides
   :hidden:

   guides/hw-development-guidelines
   guides/manufacturing-guidelines

.. toctree::
   :maxdepth: 2
   :caption: 📚 References
   :hidden:

   glossary
   license

*****
Intro
*****

Welcome to Libre Space Foundation documentation.
In these pages you can find operational principles, definitions, processes and
best practices for various internal workings of the Foundation.

* :ref:`search`
