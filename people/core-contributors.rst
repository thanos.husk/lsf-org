#################
Core Contributors
#################

Volunteer and paid contributors to LSF that have showcased substantial
contributions in one or more projects of the Foundation and alignment with the
Libre Space Manifesto, are invited by the board to participate in the "Core
Contributors" group.

Current "Core Contributors" can be found in the `About Us
<https://libre.space/about-us/>`_ section of the Libre Space website.

Being a member of "Core Contributors" grants the individual the following
privileges and duties:

#. Recognition in libre.space website About section
#. Access to private "Core Contributors" discussion group in
   community.libre.space
#. Access to #lsf-core matrix channel
#. An email account of @libre.space format
#. Membership to LSF Core group on GitLab
#. Access to cloud.libre.space
#. Participation to LSF All-Hands (annual get together of all core
   contributors)
#. Commitment to represent, spread and uphold the Principles and Pillars of the
   Libre Space Manifesto

*******
Joining
*******

Joining the "Core Contributors" group requires a nomination by an existing
member and approval by the LSF board.

**********
Onboarding
**********

Once a new member is approved for the "Core Contributors" group, the process of
onboarding can start. The first step of this process is to be assigned an
onboarding "buddy". The "buddy" will help and support you with the process,
guide you through the day-to-day processes and instill you with the values and
culture of the organization. The onboarding also includes trivial IT operations
actions as well as actions to kick-start collaboration with other
contributors. The new contributor should `open a new onboarding issue
<https://gitlab.com/librespacefoundation/lsf-org/-/issues/new?issuable_template=Contributors%20-%20Onboarding&issue[title]=Onboard%20%3CFull%20Name%3E>`_
on the LSF organizational repository, and follow the checklist to
completion. This detailed checklist contains all actions that need to happen so
that the new contributor is fully enabled.
