##################
Projects Portfolio
##################

.. contents::
   :local:
   :backlinks: none

.. _Dimitris Papadeas: https://gitlab.com/dpa2deas
.. _Eleftherios Kosmas: https://gitlab.com/elkosmas
.. _George Tsagkarelis: https://gitlab.com/georgetsag
.. _Ilias Daradimos: https://gitlab.com/drid
.. _Manolis Surligas: https://gitlab.com/surligas
.. _Manthos Papamatthaiou: https://gitlab.com/papamat
.. _Michail Raptakis: https://gitlab.com/mraptakis
.. _Nikoletta Triantafyllopoulou: https://gitlab.com/letria
.. _Pierros Papadeas: https://gitlab.com/pierros
.. _Vasilis Tsiligiannis: https://gitlab.com/acinonyx
.. _Xabi Crespo: https://gitlab.com/crespum

********
Overview
********

Libre Space Foundation develops and supports a variety of open-source projects
for space.
For each project there is a project manager and a project champion (more on
those :doc:`roles`).

*************************
Currently active projects
*************************

The following major projects are in active development:

+------------------+--------------------------+---------------------------------+
| Project          | Project Manager          | Project Champion                |
+==================+==========================+=================================+
| `SatNOGS`_       | `Vasilis Tsiligiannis`_  | `Vasilis Tsiligiannis`_         |
+------------------+--------------------------+---------------------------------+
| `tiny-ipfs`_     | `Manolis Surligas`_      | `George Tsagkarelis`_           |
+------------------+--------------------------+---------------------------------+
| `SatNOGS-tiny`_  | `Manolis Surligas`_      |                                 |
+------------------+--------------------------+---------------------------------+
| `SIDLOC`_        | `Pierros Papadeas`_      | `Vasilis Tsiligiannis`_         |
+------------------+--------------------------+---------------------------------+
| `QUBIK 3-6`_     | `Pierros Papadeas`_      | `Manthos Papamatthaiou`_        |
+------------------+--------------------------+---------------------------------+
| `PICOBUS`_       | `Manthos Papamatthaiou`_ | `Manthos Papamatthaiou`_        |
+------------------+--------------------------+---------------------------------+
| `SatNOGS COMMS`_ | `Pierros Papadeas`_      | `Pierros Papadeas`_             |
+------------------+--------------------------+---------------------------------+
| Communications   | `Eleftherios Kosmas`_    | `Nikoletta Triantafyllopoulou`_ |
+------------------+--------------------------+---------------------------------+
| Rocketry         | `Manthos Papamatthaiou`_ | `Ilias Daradimos`_              |
+------------------+--------------------------+---------------------------------+
| Infrastructure   | `Vasilis Tsiligiannis`_  | `Vasilis Tsiligiannis`_         |
+------------------+--------------------------+---------------------------------+
| `Polaris`_       | `Xabi Crespo`_           | `Eleftherios Kosmas`_           |
+------------------+--------------------------+---------------------------------+

.. _SatNOGS: https://gitlab.com/groups/librespacefoundation/satnogs/
.. _OpenSatCom: https://opensatcom.org
.. _tiny-ipfs: 
.. _SatNOGS-tiny:
.. _SIDLOC:
.. _QUBIK 3-6:
.. _PICOBUS:
.. _SatNOGS COMMS:

.. _Polaris: https://gitlab.com/librespacefoundation/polaris

Other projects can be found in lsf-core repository.

***********************
Administrative projects
***********************

The following projects are administrative projects running vertically across
Libre Space Foundation:

+-----------------------------+--------------------------+--------------------------+
| Project                     | Project Manager          | Project Champion         |
+=============================+==========================+==========================+
| `Business Development`_     | `Manthos Papamatthaiou`_ | `Pierros Papadeas`_      |
+-----------------------------+--------------------------+--------------------------+
| `Facilities Management`_    | `Dimitris Papadeas`_     | `Dimitris Papadeas`_     |
+-----------------------------+--------------------------+--------------------------+
| `Financial and Accounting`_ | `Dimitris Papadeas`_     | `Pierros Papadeas`_      |
+-----------------------------+--------------------------+--------------------------+
| `Human Resources`_          | `Vasilis Tsiligiannis`_  | `Vasilis Tsiligiannis`_  |
+-----------------------------+--------------------------+--------------------------+
| `Legal Operations`_         | `Pierros Papadeas`_      | `Pierros Papadeas`_      |
+-----------------------------+--------------------------+--------------------------+
| `Launches`_                 | `Manthos Papamatthaiou`_ | `Manthos Papamatthaiou`_ |
+-----------------------------+--------------------------+--------------------------+

.. _Business Development: https://gitlab.com/librespacefoundation/lsf-bd-org
.. _Facilities Management: https://gitlab.com/librespacefoundation/lsf-fm-org
.. _Financial and Accounting: https://gitlab.com/librespacefoundation/lsf-acct-org
.. _Human Resources: https://gitlab.com/librespacefoundation/lsf-hr
.. _Legal Operations: https://gitlab.com/librespacefoundation/lsf-legal-org
.. _Launches: https://gitlab.com/librespacefoundation/lsf-launches-org

**************************
Previously active projects
**************************

The following major projects have previously been in active development:

+---------------+---------------------+------------------+
| Project       | Project Manager     | Project Champion |
+===============+=====================+==================+
| `OpenSatCom`_ | `Pierros Papadeas`_ | N/A              |
+---------------+---------------------+------------------+
| UPSat_        | `Pierros Papadeas`_ | N/A              |
+---------------+---------------------+------------------+


.. _UPSat: https://gitlab.com/groups/librespacefoundation/satnogs/_client-and-network/-/shared

************
Contributors
************

Libre Space Foundation is an active and welcoming community around open source
space projects.
We welcome all contributors in our projects and repositories.
We recognize those contributors that have been making considerable
contributions in our projects by inviting them in our "LSF Core Contributors"
group.
Learn more about :doc:`../people/core-contributors`.
